#include <stdlib.h>
#include <SPI.h>
#include <Ethernet.h>

 
int cld1Pin = 4;            // Card status pin
int rdtPin = 2;             // Data pin
int reading = 0;            // Reading status
volatile int buffer[400];   // Buffer for data
volatile int i = 0;         // Buffer counter
volatile int bit = 0;       // global bit
char cardData[40];          // holds card info
int charCount = 0;          // counter for info
int DEBUG = 0;
String DOB;                 //Holds date of birth
char credCardNum[40];
String CCN;                 // Holds Credit Card Number
int pass=0;
int id=2525;
bool startread=false;
char response[32];
int pos=0;
int ledPin=8;

void receivedata();
// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {  0x90, 0xA2, 0xDA, 0x00, 0xE0, 0x0D };
IPAddress server(198,23,57,74); // owldrinks.com

// Initialize the Ethernet client library
// with the IP address and port of the server 
// that you want to connect to (port 80 is default for HTTP):
EthernetClient client;

void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  
  // The interrupts are key to reliable
  // reading of the clock and data feed
  attachInterrupt(0, changeBit, CHANGE);
  attachInterrupt(1, writeBit, FALLING);
  pinMode(8, OUTPUT);
 

// start the Ethernet connection:
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // no point in carrying on, so do nothing forevermore:
}
  // give the Ethernet shield a second to initialize:
  delay(1000);
  Serial.println("connecting...");

}//end of setup function



 
void loop()
{
  // if there are incoming bytes available 
  // from the server, read them and print them:
  if (client.connect(server,80)) {
    Serial.println("CONNECTED");
    Serial.println(pass);
    Serial.print("");
    Serial.print(CCN);
//    Serial.print(pass);  
    Serial.println("");
    //Serial.print(CCN);
    Serial.println("");
    client.print("GET http://www.owldrinks.com/validation.php?pass=");
    client.print(pass);
    client.print("&CCN=");
    client.print(CCN);
    client.println(" HTTP/1.0");
    client.println();
  } else {
    Serial.println("connection failed");
  }
  client.stop();
  while(client.status()!=0)
  {
    delay(5);
  }   
 
  // Active when card present
  while(digitalRead(cld1Pin) == LOW){
    reading = 1;
  }     
 
  // Active when read is complete
  // Reset the buffer
  if(reading == 1) {  
 
    if (DEBUG == 1) {
      printBuffer();
    }
 
    decode();
    reading = 0;
    i = 0;
 
    int l;
    for (l = 0; l < 40; l = l + 1) {
      cardData[l] = '\n';
    }
 
    charCount = 0;
  }
  receivedata();
  
}//end of void loop function
 
// Flips the global bit
void changeBit(){
  if (bit == 0) {
    bit = 1;
  } else {
    bit = 0;
  }
}
 
// Writes the bit to the buffer
void writeBit(){
  buffer[i] = bit;
  i++;
}
 
// prints the buffer
void printBuffer(){
  int j;
  for (j = 0; j < 200; j = j + 1) {
    Serial.println(buffer[j]);
  }
}
 
int getStartSentinal(){
  int j;
  int queue[5];
  int sentinal = 0;
 
  for (j = 0; j < 400; j = j + 1) {
    queue[4] = queue[3];
    queue[3] = queue[2];
    queue[2] = queue[1];
    queue[1] = queue[0];
    queue[0] = buffer[j];
 
    if (DEBUG == 1) {
      Serial.print(queue[0]);
      Serial.print(queue[1]);
      Serial.print(queue[2]);
      Serial.print(queue[3]);
      Serial.println(queue[4]);
    }
 
    if (queue[0] == 0 & queue[1] == 1 & queue[2] == 0 & queue[3] == 1 & queue[4] == 1) {
      sentinal = j - 4;
      break;
    }
  }
 
  if (DEBUG == 1) {
    Serial.print("sentinal:");
    Serial.println(sentinal);
    Serial.println("");
  }
 
  return sentinal;
}
 
void decode() {
  int sentinal = getStartSentinal();
  int j;
  int i = 0;
  int k = 0;
  int thisByte[5];
  int l=1;
 
  for (j = sentinal; j < 400 - sentinal; j = j + 1) {
    thisByte[i] = buffer[j];
    i++;
    if (i % 5 == 0) {
      i = 0;
      if (thisByte[0] == 0 & thisByte[1] == 0 & thisByte[2] == 0 & thisByte[3] == 0 & thisByte[4] == 0) {
        break;
      }
      printMyByte(thisByte);
    }
  }
 

  for (k = 0; k < charCount; k = k + 1) 
  {
    credCardNum[k]=cardData[k];
   
  }
  
  if(credCardNum[17]=='=')
  {
      CCN=String(credCardNum[1])+String(credCardNum[2])+String(credCardNum[3])+String(credCardNum[4])+String(credCardNum[5])+String(credCardNum[6])+String(credCardNum[7])+String(credCardNum[8])+String(credCardNum[9])+String(credCardNum[10])+String(credCardNum[11])+String(credCardNum[12])+String(credCardNum[13])+String(credCardNum[14])+String(credCardNum[15])+String(credCardNum[16]);
     Serial.print(CCN); 
  }
  else
  {
  unsigned long num=0;
  unsigned long date=0;
  unsigned long age=0;  
  DOB=String(cardData[25])+String(cardData[26])+String(cardData[27])+String(cardData[28])+String(cardData[23])+String(cardData[24])+String(cardData[31])+String(cardData[32]);
  for (int i = 0; i < 8; i++) 
  {
    num = num * 10 + (DOB[i] - '0');
  }
  
  
  
  //unsigned long date = digitalClockDisplay();
  date=20130407;
  age=date-num;
  
  if (age >=210000)
  {
    pass=1;
  }
  else 
  {
    pass=2;
  }
  }
}
 
void printMyByte(int thisByte[]) {
  int i;
  for (i = 0; i < 5; i = i + 1) {
    if (DEBUG == 1) {
      Serial.print(thisByte[i]);
    }
}
    if (DEBUG == 1) {
      Serial.print("\t");
      Serial.print(decodeByte(thisByte));
      Serial.println("");
    }
 
    cardData[charCount] = decodeByte(thisByte);
    charCount ++;
}
 
char decodeByte(int thisByte[]) {
    if (thisByte[0] == 0 & thisByte[1] == 0 & thisByte[2] == 0 & thisByte[3] == 0 & thisByte[4] == 1){
      return '0';
    }
    if (thisByte[0] == 1 & thisByte[1] == 0 & thisByte[2] == 0 & thisByte[3] == 0 & thisByte[4] == 0){
      return '1';
    }
 
    if (thisByte[0] == 0 & thisByte[1] == 1 & thisByte[2] == 0 & thisByte[3] == 0 & thisByte[4] == 0){
      return '2';
    }
 
    if (thisByte[0] == 1 & thisByte[1] == 1 & thisByte[2] == 0 & thisByte[3] == 0 & thisByte[4] == 1){
      return '3';
    }
 
    if (thisByte[0] == 0 & thisByte[1] == 0 & thisByte[2] == 1 & thisByte[3] == 0 & thisByte[4] == 0){
      return '4';
    }
 
    if (thisByte[0] == 1 & thisByte[1] == 0 & thisByte[2] == 1 & thisByte[3] == 0 & thisByte[4] == 1){
      return '5';
    }
 
    if (thisByte[0] == 0 & thisByte[1] == 1 & thisByte[2] == 1 & thisByte[3] == 0 & thisByte[4] == 1){
      return '6';
    }
 
    if (thisByte[0] == 1 & thisByte[1] == 1 & thisByte[2] == 1 & thisByte[3] == 0 & thisByte[4] == 0){
      return '7';
    }
 
    if (thisByte[0] == 0 & thisByte[1] == 0 & thisByte[2] == 0 & thisByte[3] == 1 & thisByte[4] == 0){
      return '8';
    }
 
    if (thisByte[0] == 1 & thisByte[1] == 0 & thisByte[2] == 0 & thisByte[3] == 1 & thisByte[4] == 1){
      return '9';
    }
 
    if (thisByte[0] == 0 & thisByte[1] == 1 & thisByte[2] == 0 & thisByte[3] == 1 & thisByte[4] == 1){
      return ':';
    }
 
    if (thisByte[0] == 1 & thisByte[1] == 1 & thisByte[2] == 0 & thisByte[3] == 1 & thisByte[4] == 0){
      return ';';
    }
 
    if (thisByte[0] == 0 & thisByte[1] == 0 & thisByte[2] == 1 & thisByte[3] == 1 & thisByte[4] == 1){
      return '<';
    }
 
    if (thisByte[0] == 1 & thisByte[1] == 0 & thisByte[2] == 1 & thisByte[3] == 1 & thisByte[4] == 0){
      return '=';
    }
 
    if (thisByte[0] == 0 & thisByte[1] == 1 & thisByte[2] == 1 & thisByte[3] == 1 & thisByte[4] == 0){
      return '>';
    }
 
    if (thisByte[0] == 1 & thisByte[1] == 1 & thisByte[2] == 1 & thisByte[3] == 1 & thisByte[4] == 1){
      return '?';
    }
}
void receivedata()
{
 // Serial.println(Ethernet.localIP());
  if (client.connect(server, 80)) {
    Serial.println("connected");
    client.print("GET http://www.owldrinks.com/fetch.php?id=");
    client.print(id);
    client.println(" HTTP/1.0");
    client.println();
  } else {
    //if connection fails, call ethernet.begin()
    Serial.println("connection failed");
  }
  delay(5);
  while(true)
  {
 while (client.available()) {
    char c = client.read();
    if(c == '<')
    {
      startread=true;
    }else if(startread){
      if(c!='>')
      {
        response[pos]=c;
        pos++;
       // Serial.print(c);
      } 
      else
      {
        pos=0;
        startread=false;
       // Serial.println(response[0]);
        //here call the function to turn on/off relays
        makeDrink();
      }
  }
 }
 // for(;;);
  delay(5);
  //receivedata();
  if(!client.connected())
  {
    client.stop();
    break;
  }
  }     
  
}


void makeDrink()
{
 
  Serial.println(response[0]);
  
  delay(5);
 
  if(response[0]=='a')
  {
    Serial1.print('a'); //User selected Cranberry Juice
    delay(4000);
    Serial1.print('z');
  }
  else if(response[0]=='b')
  {
    Serial1.print('b'); //User selected Orange Juice
    delay(4000);
   Serial1.print('z');
  }
  else if(response[0]=='c')
  {
    Serial1.print('c'); //User selected Vodka on the rocks
    delay(4000);
    Serial1.print('z');
  }
  else if(response[0]=='d')
  {
    Serial1.print('d'); //User selected Pure Rum 
    delay(4000);
    Serial1.print('z');
  }
  else if(response[0]=='e')
  {
    Serial1.print('e'); //User selected Tequila Shot
    delay(4000);
    Serial1.print('z');
  }
  else if(response[0]=='f')
  {
    Serial1.print('f'); //User selected Cape Codder
    delay(4000);
    Serial1.print('z');
  }
  else if(response[0]=='g')
  {
    Serial1.print('g'); //User selected Lemon Drop
    delay(4000);
   Serial1.print('z');
  }
   else if(response[0]=='h')
  {
    Serial1.print('h'); //User selected The Madrras
    delay(4000);
    Serial1.print('z');
  }
  else if(response[0]=='i')
  {
    Serial1.print('i'); //User selected The Mexi-tini
    delay(4000);
    Serial1.print('z');
  }
  else if(response[0]=='j')
  {
    Serial1.print('j'); //User selected Screwdriver
    delay(4000);
    Serial1.print('z');
   
  }
  else if(response[0]=='k')
  {
    Serial1.print('k'); //User selected Margarita
    delay(4000);
    Serial1.print('z');
  }
  else if(response[0]=='l')
  {
    Serial1.print('l'); //User selected Horny Bull
    delay(4000);
    Serial1.print('z');
   
  }
  else if(response[0]=='m')
  {
    Serial1.print('m'); //User selected The Pepa Drink
    delay(4000);
    Serial1.print('z');
  }
  
 
  
}
