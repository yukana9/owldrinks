int vodka = 2;               // valve 1 (Vodka) is hoooked up to pin 8
int orangeJuice = 3;        // valve 2 (Orange Juice) is hoooked up to pin 9
int tequila = 5;           // valve 3 (Tequila) is hoooked up to pin 10
int cranberryJuice = 6;   // valve 4 (Cranberry Juice) is hoooked up to pin 11
int rum = 7;             // valve 5 (Rum) is hoooked up to pin 12
int lemonJuice = 8;    // valve 6 (Lemon Juice) is hoooked up to pin 13
int analogPin=A1;

void setup() 
{
  
  Serial.begin(9600);
  pinMode(vodka, OUTPUT);
  digitalWrite(vodka, LOW);
  
  pinMode(orangeJuice, OUTPUT);
  digitalWrite(orangeJuice, LOW);
  
  pinMode(tequila, OUTPUT);
  digitalWrite(tequila, LOW);
  
  pinMode(cranberryJuice, OUTPUT);
  digitalWrite(cranberryJuice, LOW);
  
  pinMode(rum, OUTPUT);
  digitalWrite(rum, LOW);
  
  pinMode(lemonJuice, OUTPUT);
  digitalWrite(lemonJuice, LOW);
}

void loop()
{
  int reading = analogRead(analogPin);
  Serial.print("Sensor works: ");
  Serial.print(reading);  
  char recievedchar=Serial.read();
  Serial.print(recievedchar);
  Serial.println();
  delay(1000);
  if(recievedchar=='a')// Cranberry Juice (Cranberry Juice and Ice)
  {
    if(reading<800){
    digitalWrite(cranberryJuice, HIGH);   // sets the valve on
    delay(20000);                        // waits for 20 seconds
    digitalWrite(cranberryJuice, LOW);   // sets the valve off
    recievedchar='z';
    }
}
  else if((recievedchar=='b'))//Orange Juice (Orange Juice)
  {
    if(reading<800){
    digitalWrite(orangeJuice, HIGH);   // sets the valve on
    delay(20000);                  // waits for 20 seconds
    digitalWrite(orangeJuice, LOW);    // sets the valve off
     recievedchar='z';
    }
  }
  else if(recievedchar=='c')// Vodka on the rocks(Vodka and Ice)
  {
    if(reading<800){
    digitalWrite(vodka, HIGH);   // sets the valve on
    delay(5000);                  // waits for 5 seconds
    digitalWrite(vodka, LOW);    // sets the valve off
     recievedchar='z';
  }
  }
  
  else if(recievedchar=='d')//Pure RumRum)
  {
    if(reading<800){
    digitalWrite(rum, HIGH);   // sets the valve on
    delay(10000);                  // waits for 10 seconds
    digitalWrite(rum, LOW);    // sets the valve off
     recievedchar='z';
    }
  }
  
  else if(recievedchar=='e') // Tequila Shot (Tequila)
  {
    if(reading<800){
    Serial.print("TEQUILA");
    digitalWrite(tequila, HIGH);   // sets the valve on
    delay(5000);                  // waits for 5 seconds
    digitalWrite(tequila, LOW);    // sets the valve off
     recievedchar='z';
    }
 }
  
  else if(recievedchar=='f') // Cape Codder (Vodka, Cranberry Juice, Lime)
  {if(reading<800){
    digitalWrite(vodka, HIGH);   // sets the valve on
    delay(10000);                  // waits for 10 seconds
    digitalWrite(vodka, LOW);    // sets the valve off
     
    digitalWrite(cranberryJuice, HIGH);   // sets the valve on
    delay(10000);                  // waits for 10 seconds
    digitalWrite(cranberryJuice, LOW);    // sets the valve off
     recievedchar='z';
  }
  }
   else if(recievedchar=='g') // Lemon Drop (Vodka, Lemon Juice,Sugar)
  {if(reading<800){
    digitalWrite(vodka, HIGH);   // sets the valve on
    delay(10000);                  // waits for 10 seconds
    digitalWrite(vodka, LOW);    // sets the valve off
     
    digitalWrite(lemonJuice, HIGH);   // sets the valve on
    delay(10000);                  // waits for 10 seconds
    digitalWrite(lemonJuice, LOW);    // sets the valve off
     recievedchar='z';
  }
  }
   else if(recievedchar=='h') // The Madrras (Vodka, Cranberry and Orange Juice)
  {
    if(reading<800){
    digitalWrite(vodka, HIGH);   // sets the valve on
    delay(7000);                  // waits for 7 seconds
    digitalWrite(vodka, LOW);    // sets the valve off
    
    digitalWrite(orangeJuice, HIGH);   // sets the valve on
    delay(7000);                  // waits for 7 seconds
    digitalWrite(orangeJuice, LOW);    // sets the valve off
    
    digitalWrite(cranberryJuice, HIGH);   // sets the valve on
    delay(7000);                  // waits for 7 seconds
    digitalWrite(cranberryJuice, LOW);    // sets the valve off
     recievedchar='z';
  }
  }
   else if(recievedchar=='i') // The Mexi-tini(Vodka, tequila, orange juice)
  { 
    if(reading<800){
    digitalWrite(vodka, HIGH);   // sets the valve on
    delay(7000);                  // waits for 7 seconds
    digitalWrite(vodka, LOW);    // sets the valve off
    
    digitalWrite(orangeJuice, HIGH);   // sets the valve on
    delay(7000);                  // waits for 7 seconds
    digitalWrite(orangeJuice, LOW);    // sets the valve off
    
    digitalWrite(tequila, HIGH);   // sets the valve on
    delay(7000);                  // waits for 7 seconds
    digitalWrite(tequila, LOW);    // sets the valve off
     recievedchar='z';
    }
  }
   else if(recievedchar=='j') //  Screwdriver(Vodka and orange juice)
  {
    if(reading<800){
    digitalWrite(vodka, HIGH);   // sets the valve on
    delay(10000);                  // waits for 10 seconds
    digitalWrite(vodka, LOW);    // sets the valve off
    
    digitalWrite(orangeJuice, HIGH);   // sets the valve on
    delay(10000);                  // waits for 10 seconds
    digitalWrite(orangeJuice, LOW);    // sets the valve off
     recievedchar='z';
    }  
  }
   else if(recievedchar=='k') //  Margarita (Tequila and lemon juice)
  {
    if(reading<800){
    digitalWrite(tequila, HIGH);   // sets the valve on
    delay(10000);                  // waits for 10 seconds
    digitalWrite(tequila, LOW);    // sets the valve off
    
    digitalWrite(lemonJuice, HIGH);   // sets the valve on
    delay(10000);                  // waits for 10 seconds
    digitalWrite(lemonJuice, LOW);    // sets the valve off
     recievedchar='z';
    }
 }
   else if(recievedchar=='l')//  Horny Bull (1/2 tequila 1/2 rum)
  {
    if(reading<800){
    digitalWrite(tequila, HIGH);   // sets the valve on
    delay(10000);                  // waits for 10 seconds
    digitalWrite(tequila, LOW);    // sets the valve off
    
    digitalWrite(rum, HIGH);   // sets the valve on
    delay(10000);                  // waits for 10 seconds
    digitalWrite(rum, LOW);    // sets the valve off
     recievedchar='z';
  }
  }
   else if(recievedchar=='m') // The Pepa Drink (Orange juice, cranberry juice)
  {
    if(reading<800){
    digitalWrite(orangeJuice, HIGH);   // sets the valve on
    delay(10000);                  // waits for 10 seconds
    digitalWrite(orangeJuice, LOW);    // sets the valve off
    
    digitalWrite(cranberryJuice, HIGH);   // sets the valve on
    delay(10000);                  // waits for 10 seconds
    digitalWrite(cranberryJuice, LOW);    // sets the valve off
     recievedchar='z';
    }
  }
 else
 {
   digitalWrite(vodka, LOW);
   digitalWrite(orangeJuice, LOW);
   digitalWrite(tequila, LOW);
   digitalWrite(cranberryJuice, LOW);
   digitalWrite(rum, LOW);
   digitalWrite(lemonJuice, LOW);
   recievedchar='z';
     
 }
  recievedchar='z';
}
